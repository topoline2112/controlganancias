<?php

namespace Prueba\Http\Controllers;

use Illuminate\Support\Facades\DB; //Base de datos la clase
use Illuminate\Http\Request;


class ControladorReportes extends Controller
{
    //Queda de ejemplo, borrar:
	public function imprimir_reporte_vale_entregado(Request $pedir){
    	$id_area = $pedir->input('id_area');
    	$id = $pedir->input('id');
    	$area = $pedir->input('area');   	

    	return view('SuperUser/Reportes/Vales/vales_entregados_imprimir', ['area' => $area, 'id_area' => $id_area, 'id' => $id]); 
		
	}


}
