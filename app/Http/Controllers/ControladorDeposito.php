<?php

namespace Prueba\Http\Controllers;

use Illuminate\Http\Request;
use Prueba\Http\Controllers\Controller;

class ControladorDeposito extends Controller
{
    ////RUTAS ORDENES DE COMPRA
    public function verOrdenesDeCompra()
    {
    	return view('Deposito/ordenesCompra/mostrarOrdenes');
    }

    public function verDetalleDeCompra()
    {
    	$datos = array('porcentaje' => "15");
    	return view('Deposito/ordenesCompra/detalleOrden',$datos);
    }


    //RUTAS MATERIAS PRIMA
    public function verMateriaPrima()
    {
        return view('Deposito/materiaPrima/mostrarMateriaPrima');
    }
    public function verProductos()
    {
        return view('Deposito/productos/mostrarProductos');
    }

}
