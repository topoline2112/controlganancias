<?php

namespace Prueba\Http\Controllers;

use Illuminate\Support\Facades\DB; //Base de datos la clase
use Illuminate\Http\Request;


class ControladorLogin extends Controller
{
	
	
	public function intentoLogueo(Request $pedir)
	{
		$usuario = $pedir->input('user');
		$contrasenia = $pedir->input('pass');

		 //Ejemplo, reutilizar con BD y tabla nueva (usar sesiones)
		$consulta = DB::table('login')->select('*')->where('usuario', $usuario)->where('clave', $contrasenia)->get();

		if (count($consulta) > 0) 
		{  
			//El valor que trae estado hace referencia a si esta bloqueado o no el usuario
			if($consulta[0]->estado == 1) // 1 referencia que no esta bloqueado
			{
				//Obtenemos los perfiles del usuario
				$perfiles = DB::table('perfil_login')
				->join('perfil','perfil.idperfil','=','perfil_login.idperfil')
				->select('perfil_login.*','perfil.*')
				->where('perfil_login.idlogin','=',$consulta[0]->idlogin)
				->get();

				session(['idpersona' => $consulta[0]->idpersona]);

				
				
				return view('panel',['perfiles' => $perfiles]); 

			}
			else
			{
				$logIncorrecto = 1;
				return view('login', ['logIncorrecto' => $logIncorrecto]); 
			}
			
		}else{
			session_start();
			$logIncorrecto = 1;
			return view('login', ['logIncorrecto' => $logIncorrecto]); 
		}
	}

	public function logout(){
		session()->flush();
		return view('login'); 
	}

	public function bienvenida(){
		
		return view('bienvenida'); 
	}

}
