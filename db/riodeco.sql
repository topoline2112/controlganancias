-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-05-2019 a las 19:40:52
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `riodeco`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consumo_detalleof`
--

CREATE TABLE `consumo_detalleof` (
  `idconsumo_detalleOF` int(11) NOT NULL,
  `iddetalle_ordenF` int(11) DEFAULT NULL,
  `idmateria_prima` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_incidenteof`
--

CREATE TABLE `detalle_incidenteof` (
  `iddetalle_incidenteOF` int(11) NOT NULL,
  `idincidente_ordenF` int(11) DEFAULT NULL,
  `idmateria_prima` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ingresomp`
--

CREATE TABLE `detalle_ingresomp` (
  `iddetalle_ingresoMP` int(11) NOT NULL,
  `idingreso_materiaP` int(11) DEFAULT NULL,
  `idmateria_prima` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ordenf`
--

CREATE TABLE `detalle_ordenf` (
  `iddetalle_ordenF` int(11) NOT NULL,
  `idorden_fabricacion` int(11) DEFAULT NULL,
  `idproducto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `terminados` int(11) DEFAULT NULL,
  `retirados` int(11) DEFAULT NULL,
  `idusuario_fabrica` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_presupuesto`
--

CREATE TABLE `detalle_presupuesto` (
  `iddetalle_presupuesto` int(11) NOT NULL,
  `idpresupuesto` int(11) DEFAULT NULL,
  `idproducto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_recetap`
--

CREATE TABLE `detalle_recetap` (
  `iddetalle_recetaP` int(11) NOT NULL,
  `idreceta_producto` int(11) DEFAULT NULL,
  `idmateria_prima` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_remito`
--

CREATE TABLE `detalle_remito` (
  `iddetalle_remito` int(11) NOT NULL,
  `idremito` int(11) DEFAULT NULL,
  `idproducto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio_persona`
--

CREATE TABLE `domicilio_persona` (
  `iddomicilio_persona` int(11) NOT NULL,
  `idpersona` int(11) DEFAULT NULL,
  `calle` varchar(45) DEFAULT NULL,
  `nroCasa` int(5) DEFAULT NULL,
  `descripcion` varchar(145) DEFAULT NULL,
  `barrio` varchar(45) DEFAULT NULL,
  `latitud` decimal(3,2) DEFAULT NULL,
  `longitud` decimal(3,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `domicilio_persona`
--

INSERT INTO `domicilio_persona` (`iddomicilio_persona`, `idpersona`, `calle`, `nroCasa`, `descripcion`, `barrio`, `latitud`, `longitud`) VALUES
(1, NULL, 'Rivadavia', 1040, 'Casa blanca de rejas negras', 'San Roman', '9.99', '9.99'),
(2, NULL, 'Rivadavia', 1040, 'Casa blanca de rejas negras', 'San Roman', '9.99', '9.99');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidente_ordenf`
--

CREATE TABLE `incidente_ordenf` (
  `idincidente_ordenF` int(11) NOT NULL,
  `idseguimiento_ordenF` int(11) DEFAULT NULL,
  `idorden_fabricacion` int(11) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidente_remito`
--

CREATE TABLE `incidente_remito` (
  `idincidente_remito` int(11) NOT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `idremito` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso_materiap`
--

CREATE TABLE `ingreso_materiap` (
  `idingreso_materiaP` int(11) NOT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `descripcion` varchar(145) DEFAULT NULL,
  `idusuario_carga` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `idlogin` int(11) NOT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `idpersona` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`idlogin`, `usuario`, `clave`, `idpersona`, `estado`) VALUES
(2, 'Facu', '123', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia_prima`
--

CREATE TABLE `materia_prima` (
  `idmateria_prima` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `idtipo_unidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_fabricacion`
--

CREATE TABLE `orden_fabricacion` (
  `idorden_fabricacion` int(11) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `idusuario_crea` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `idperfil` int(11) NOT NULL,
  `nombre` varchar(25) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`idperfil`, `nombre`, `descripcion`, `estado`) VALUES
(1, 'Deposito', 'Encargado del deposito', '1'),
(2, 'Planta', 'Encargado de la planta', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_login`
--

CREATE TABLE `perfil_login` (
  `idperfil_login` int(11) NOT NULL,
  `idlogin` int(11) DEFAULT NULL,
  `idperfil` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfil_login`
--

INSERT INTO `perfil_login` (`idperfil_login`, `idlogin`, `idperfil`) VALUES
(1, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `dni` int(9) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `idtipo_persona` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `nombre`, `apellido`, `dni`, `telefono`, `email`, `idtipo_persona`) VALUES
(1, 'Mauricio', 'Lahitte', 37739860, '3804316087', 'mauricio.mm94@gmail.com', 1),
(2, 'Facundo', 'Aumada', 38481233, '3804394928', 'aufacun21w@gmail.com', 1),
(3, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE `presupuesto` (
  `idpresupuesto` int(11) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `idusuario_crea` int(11) DEFAULT NULL,
  `idcliente` int(11) DEFAULT NULL,
  `idorden_fabricacion` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `presupuesto`
--

INSERT INTO `presupuesto` (`idpresupuesto`, `fecha_creacion`, `fecha_vencimiento`, `idusuario_crea`, `idcliente`, `idorden_fabricacion`, `estado`, `total`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, '20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `idtipo_unidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receta_prodcuto`
--

CREATE TABLE `receta_prodcuto` (
  `idreceta_prodcuto` int(11) NOT NULL,
  `idproducto` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `remito`
--

CREATE TABLE `remito` (
  `idremito` int(11) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `idorden_fabricacion` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguimiento_ordenf`
--

CREATE TABLE `seguimiento_ordenf` (
  `idseguimiento_ordenF` int(11) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `descripcion` varchar(145) DEFAULT NULL,
  `idusuario_crea` int(11) DEFAULT NULL,
  `idorden_fabricacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_materiap`
--

CREATE TABLE `stock_materiap` (
  `idstock_materiaP` int(11) NOT NULL,
  `idmateria_prima` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_producto`
--

CREATE TABLE `stock_producto` (
  `idstock_producto` int(11) NOT NULL,
  `idproducto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_persona`
--

CREATE TABLE `tipo_persona` (
  `idtipo_persona` int(11) NOT NULL,
  `nombre` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_persona`
--

INSERT INTO `tipo_persona` (`idtipo_persona`, `nombre`) VALUES
(1, 'Empleado'),
(2, 'Cliente'),
(3, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_unidad`
--

CREATE TABLE `tipo_unidad` (
  `idtipo_unidad` int(11) NOT NULL,
  `unidad` varchar(15) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `consumo_detalleof`
--
ALTER TABLE `consumo_detalleof`
  ADD PRIMARY KEY (`idconsumo_detalleOF`),
  ADD KEY `FK_consumoDOF_detalleOF_idx` (`iddetalle_ordenF`),
  ADD KEY `FK_consumoDOF_materiaP_idx` (`idmateria_prima`);

--
-- Indices de la tabla `detalle_incidenteof`
--
ALTER TABLE `detalle_incidenteof`
  ADD PRIMARY KEY (`iddetalle_incidenteOF`),
  ADD KEY `FK_detalleIOF_incidenteOF_idx` (`idincidente_ordenF`),
  ADD KEY `FK_detalleIOF_materiaP_idx` (`idmateria_prima`);

--
-- Indices de la tabla `detalle_ingresomp`
--
ALTER TABLE `detalle_ingresomp`
  ADD PRIMARY KEY (`iddetalle_ingresoMP`),
  ADD KEY `FK_detalleIMP_ingresoMP_idx` (`idingreso_materiaP`),
  ADD KEY `FK_detalleIMP_materiaP_idx` (`idmateria_prima`);

--
-- Indices de la tabla `detalle_ordenf`
--
ALTER TABLE `detalle_ordenf`
  ADD PRIMARY KEY (`iddetalle_ordenF`),
  ADD KEY `FK_detalle_ordenF_idx` (`idorden_fabricacion`),
  ADD KEY `FK_detalle_producto_idx` (`idproducto`),
  ADD KEY `FK_detalle_usuarioFabrica_idx` (`idusuario_fabrica`);

--
-- Indices de la tabla `detalle_presupuesto`
--
ALTER TABLE `detalle_presupuesto`
  ADD PRIMARY KEY (`iddetalle_presupuesto`),
  ADD KEY `FK_detallePresu_presupuesto_idx` (`idpresupuesto`),
  ADD KEY `FK_detallePresu_producto_idx` (`idproducto`);

--
-- Indices de la tabla `detalle_recetap`
--
ALTER TABLE `detalle_recetap`
  ADD PRIMARY KEY (`iddetalle_recetaP`),
  ADD KEY `FK_detalleRP_recetaP_idx` (`idreceta_producto`),
  ADD KEY `FK_detalleRP_materiaP_idx` (`idmateria_prima`);

--
-- Indices de la tabla `detalle_remito`
--
ALTER TABLE `detalle_remito`
  ADD PRIMARY KEY (`iddetalle_remito`),
  ADD KEY `FK_detalleROF_producto_idx` (`idproducto`),
  ADD KEY `FK_detalleR_remito_idx` (`idremito`);

--
-- Indices de la tabla `domicilio_persona`
--
ALTER TABLE `domicilio_persona`
  ADD PRIMARY KEY (`iddomicilio_persona`),
  ADD KEY `FK_domicilioPersona_persona_idx` (`idpersona`);

--
-- Indices de la tabla `incidente_ordenf`
--
ALTER TABLE `incidente_ordenf`
  ADD PRIMARY KEY (`idincidente_ordenF`),
  ADD KEY `FK_incidenteOF_seguiOF_idx` (`idseguimiento_ordenF`),
  ADD KEY `FK_incidenteOF_ordenF_idx` (`idorden_fabricacion`);

--
-- Indices de la tabla `incidente_remito`
--
ALTER TABLE `incidente_remito`
  ADD PRIMARY KEY (`idincidente_remito`),
  ADD KEY `FK_incidenteR_remito_idx` (`idremito`);

--
-- Indices de la tabla `ingreso_materiap`
--
ALTER TABLE `ingreso_materiap`
  ADD PRIMARY KEY (`idingreso_materiaP`),
  ADD KEY `FK_ingresoMP_usuarioCarga_idx` (`idusuario_carga`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`idlogin`),
  ADD KEY `FK_login_persona_idx` (`idpersona`);

--
-- Indices de la tabla `materia_prima`
--
ALTER TABLE `materia_prima`
  ADD PRIMARY KEY (`idmateria_prima`),
  ADD KEY `FK_materiaP_tipoUnidad_idx` (`idtipo_unidad`);

--
-- Indices de la tabla `orden_fabricacion`
--
ALTER TABLE `orden_fabricacion`
  ADD PRIMARY KEY (`idorden_fabricacion`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`idperfil`);

--
-- Indices de la tabla `perfil_login`
--
ALTER TABLE `perfil_login`
  ADD PRIMARY KEY (`idperfil_login`),
  ADD KEY `FK_pl_login_idx` (`idlogin`),
  ADD KEY `FK_pl_perfil_idx` (`idperfil`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`),
  ADD KEY `FK_persona_tipoP_idx` (`idtipo_persona`);

--
-- Indices de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD PRIMARY KEY (`idpresupuesto`),
  ADD KEY `FK_presupuesto_usuarioCrea_idx` (`idusuario_crea`),
  ADD KEY `FK_presupuesto_persona_idx` (`idcliente`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`),
  ADD KEY `FK_producto_tipoUnidad_idx` (`idtipo_unidad`);

--
-- Indices de la tabla `receta_prodcuto`
--
ALTER TABLE `receta_prodcuto`
  ADD PRIMARY KEY (`idreceta_prodcuto`);

--
-- Indices de la tabla `remito`
--
ALTER TABLE `remito`
  ADD PRIMARY KEY (`idremito`);

--
-- Indices de la tabla `seguimiento_ordenf`
--
ALTER TABLE `seguimiento_ordenf`
  ADD PRIMARY KEY (`idseguimiento_ordenF`),
  ADD KEY `FK_seguimientoOF_usuario_idx` (`idusuario_crea`),
  ADD KEY `FK_seguimientoOF_ordenF_idx` (`idorden_fabricacion`);

--
-- Indices de la tabla `stock_materiap`
--
ALTER TABLE `stock_materiap`
  ADD PRIMARY KEY (`idstock_materiaP`),
  ADD KEY `FK_stockMP_materiaP_idx` (`idmateria_prima`);

--
-- Indices de la tabla `stock_producto`
--
ALTER TABLE `stock_producto`
  ADD PRIMARY KEY (`idstock_producto`),
  ADD KEY `FK_stockP_producto_idx` (`idproducto`);

--
-- Indices de la tabla `tipo_persona`
--
ALTER TABLE `tipo_persona`
  ADD PRIMARY KEY (`idtipo_persona`);

--
-- Indices de la tabla `tipo_unidad`
--
ALTER TABLE `tipo_unidad`
  ADD PRIMARY KEY (`idtipo_unidad`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `consumo_detalleof`
--
ALTER TABLE `consumo_detalleof`
  MODIFY `idconsumo_detalleOF` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_incidenteof`
--
ALTER TABLE `detalle_incidenteof`
  MODIFY `iddetalle_incidenteOF` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_ingresomp`
--
ALTER TABLE `detalle_ingresomp`
  MODIFY `iddetalle_ingresoMP` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_ordenf`
--
ALTER TABLE `detalle_ordenf`
  MODIFY `iddetalle_ordenF` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_presupuesto`
--
ALTER TABLE `detalle_presupuesto`
  MODIFY `iddetalle_presupuesto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_recetap`
--
ALTER TABLE `detalle_recetap`
  MODIFY `iddetalle_recetaP` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_remito`
--
ALTER TABLE `detalle_remito`
  MODIFY `iddetalle_remito` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `domicilio_persona`
--
ALTER TABLE `domicilio_persona`
  MODIFY `iddomicilio_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `incidente_ordenf`
--
ALTER TABLE `incidente_ordenf`
  MODIFY `idincidente_ordenF` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `incidente_remito`
--
ALTER TABLE `incidente_remito`
  MODIFY `idincidente_remito` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ingreso_materiap`
--
ALTER TABLE `ingreso_materiap`
  MODIFY `idingreso_materiaP` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `materia_prima`
--
ALTER TABLE `materia_prima`
  MODIFY `idmateria_prima` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orden_fabricacion`
--
ALTER TABLE `orden_fabricacion`
  MODIFY `idorden_fabricacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `idperfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `perfil_login`
--
ALTER TABLE `perfil_login`
  MODIFY `idperfil_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idpersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  MODIFY `idpresupuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `receta_prodcuto`
--
ALTER TABLE `receta_prodcuto`
  MODIFY `idreceta_prodcuto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `remito`
--
ALTER TABLE `remito`
  MODIFY `idremito` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `seguimiento_ordenf`
--
ALTER TABLE `seguimiento_ordenf`
  MODIFY `idseguimiento_ordenF` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `stock_materiap`
--
ALTER TABLE `stock_materiap`
  MODIFY `idstock_materiaP` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `stock_producto`
--
ALTER TABLE `stock_producto`
  MODIFY `idstock_producto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_persona`
--
ALTER TABLE `tipo_persona`
  MODIFY `idtipo_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_unidad`
--
ALTER TABLE `tipo_unidad`
  MODIFY `idtipo_unidad` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `consumo_detalleof`
--
ALTER TABLE `consumo_detalleof`
  ADD CONSTRAINT `FK_consumoDOF_detalleOF` FOREIGN KEY (`iddetalle_ordenF`) REFERENCES `detalle_ordenf` (`iddetalle_ordenF`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_consumoDOF_materiaP` FOREIGN KEY (`idmateria_prima`) REFERENCES `materia_prima` (`idmateria_prima`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_incidenteof`
--
ALTER TABLE `detalle_incidenteof`
  ADD CONSTRAINT `FK_detalleIOF_incidenteOF` FOREIGN KEY (`idincidente_ordenF`) REFERENCES `incidente_ordenf` (`idincidente_ordenF`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_detalleIOF_materiaP` FOREIGN KEY (`idmateria_prima`) REFERENCES `materia_prima` (`idmateria_prima`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_ingresomp`
--
ALTER TABLE `detalle_ingresomp`
  ADD CONSTRAINT `FK_detalleIMP_ingresoMP` FOREIGN KEY (`idingreso_materiaP`) REFERENCES `ingreso_materiap` (`idingreso_materiaP`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_detalleIMP_materiaP` FOREIGN KEY (`idmateria_prima`) REFERENCES `materia_prima` (`idmateria_prima`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_ordenf`
--
ALTER TABLE `detalle_ordenf`
  ADD CONSTRAINT `FK_detalle_ordenF` FOREIGN KEY (`idorden_fabricacion`) REFERENCES `orden_fabricacion` (`idorden_fabricacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_detalle_producto` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_detalle_usuarioFabrica` FOREIGN KEY (`idusuario_fabrica`) REFERENCES `login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_presupuesto`
--
ALTER TABLE `detalle_presupuesto`
  ADD CONSTRAINT `FK_detallePresu_presupuesto` FOREIGN KEY (`idpresupuesto`) REFERENCES `presupuesto` (`idpresupuesto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_detallePresu_producto` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_recetap`
--
ALTER TABLE `detalle_recetap`
  ADD CONSTRAINT `FK_detalleRP_materiaP` FOREIGN KEY (`idmateria_prima`) REFERENCES `materia_prima` (`idmateria_prima`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_detalleRP_recetaP` FOREIGN KEY (`idreceta_producto`) REFERENCES `receta_prodcuto` (`idreceta_prodcuto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_remito`
--
ALTER TABLE `detalle_remito`
  ADD CONSTRAINT `FK_detalleR_producto` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_detalleR_remito` FOREIGN KEY (`idremito`) REFERENCES `remito` (`idremito`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `domicilio_persona`
--
ALTER TABLE `domicilio_persona`
  ADD CONSTRAINT `FK_domicilioPersona_persona` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `incidente_ordenf`
--
ALTER TABLE `incidente_ordenf`
  ADD CONSTRAINT `FK_incidenteOF_ordenF` FOREIGN KEY (`idorden_fabricacion`) REFERENCES `orden_fabricacion` (`idorden_fabricacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_incidenteOF_seguiOF` FOREIGN KEY (`idseguimiento_ordenF`) REFERENCES `seguimiento_ordenf` (`idseguimiento_ordenF`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `incidente_remito`
--
ALTER TABLE `incidente_remito`
  ADD CONSTRAINT `FK_incidenteR_remito` FOREIGN KEY (`idremito`) REFERENCES `remito` (`idremito`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ingreso_materiap`
--
ALTER TABLE `ingreso_materiap`
  ADD CONSTRAINT `FK_ingresoMP_usuarioCarga` FOREIGN KEY (`idusuario_carga`) REFERENCES `login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `FK_login_persona` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `materia_prima`
--
ALTER TABLE `materia_prima`
  ADD CONSTRAINT `FK_materiaP_tipoUnidad` FOREIGN KEY (`idtipo_unidad`) REFERENCES `tipo_unidad` (`idtipo_unidad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfil_login`
--
ALTER TABLE `perfil_login`
  ADD CONSTRAINT `FK_pl_login` FOREIGN KEY (`idlogin`) REFERENCES `login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_pl_perfil` FOREIGN KEY (`idperfil`) REFERENCES `perfil` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `FK_persona_tipoP` FOREIGN KEY (`idtipo_persona`) REFERENCES `tipo_persona` (`idtipo_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD CONSTRAINT `FK_presupuesto_persona` FOREIGN KEY (`idcliente`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_presupuesto_usuarioCrea` FOREIGN KEY (`idusuario_crea`) REFERENCES `login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `FK_producto_tipoUnidad` FOREIGN KEY (`idtipo_unidad`) REFERENCES `tipo_unidad` (`idtipo_unidad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `seguimiento_ordenf`
--
ALTER TABLE `seguimiento_ordenf`
  ADD CONSTRAINT `FK_seguimientoOF_ordenF` FOREIGN KEY (`idorden_fabricacion`) REFERENCES `orden_fabricacion` (`idorden_fabricacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_seguimientoOF_usuario` FOREIGN KEY (`idusuario_crea`) REFERENCES `login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `stock_materiap`
--
ALTER TABLE `stock_materiap`
  ADD CONSTRAINT `FK_stockMP_materiaP` FOREIGN KEY (`idmateria_prima`) REFERENCES `materia_prima` (`idmateria_prima`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `stock_producto`
--
ALTER TABLE `stock_producto`
  ADD CONSTRAINT `FK_stockP_producto` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
