<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{{URL::to('/resources/template/img/favicon.png')}}">

    <title>Rio Deco - E.R.P.</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::to('/resources/template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('/resources/template/css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{URL::to('/resources/template/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{URL::to('/resources/template/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::to('/resources/template/css/style-responsive.css')}}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" action="inicio" method="POST">         
        <!-- // @crfs -->
        <h2 class="form-signin-heading"><b>Rio</b>Deco</h2>
        <div class="login-wrap">
             <?php if (isset($logIncorrecto)): ?>
                <p style="color: red"><small>Datos de ingreso incorrectos</small></p>
            <?php endif ?>
            <input type="text" name="user" class="form-control" placeholder="Usuario" autofocus>
            <input type="password" name="pass" class="form-control" placeholder="Contraseña">
            
            <a href="inicio2"><button class="btn btn-lg btn-login btn-block" type="submit">Ingresar</button></a>

        </div>
        
      </form>

      <div align="center"><p align="center"></p>V 1.2.0</div>

    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{URL::to('/resources/template/js/jquery.js')}}"></script>
    <script src="{{URL::to('/resources/template/js/bootstrap.min.js')}}"></script>


  </body>
</html>
