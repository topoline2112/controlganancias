<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>@yield('tituloPantalla')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::to('/resources/template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('/resources/template/css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{URL::to('/resources/template/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />

    @yield('seccionCss')

    
    <!-- Custom styles for this template -->
    <link href="{{URL::to('/resources/template/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::to('/resources/template/css/style-responsive.css')}}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  @yield('seccionBody')

    <!-- js placed at the end of the document so the pages load faster -->
    <!--<script src="js/jquery.js"></script>-->
    <script type="text/javascript" language="javascript" src="{{URL::to('/resources/template/assets/advanced-datatable/media/js/jquery.js')}}"></script>
    <script src="{{URL::to('/resources/template/js/bootstrap.min.js')}}"></script>
    <script class="include" type="text/javascript" src="{{URL::to('/resources/template/js/jquery.dcjqaccordion.2.7.js')}}"></script>
    <script src="{{URL::to('/resources/template/js/jquery.scrollTo.min.js')}}"></script>
    <script src="{{URL::to('/resources/template/js/jquery.nicescroll.js')}}" type="text/javascript"></script>

    <script src="{{URL::to('/resources/template/js/respond.min.js')}}" ></script>


  <!--common script for all pages-->
    <script src="{{URL::to('/resources/template/js/common-scripts.js')}}"></script>

  @yield('seccionScripts')

    
  </body>
</html>

