<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{{URL::to('/resources/template/img/favicon.png')}}">

    <title>Inicio - Rio Deco</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::to('/resources/template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('/resources/template/css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{URL::to('/resources/template/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{URL::to('/resources/template/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::to('/resources/template/css/style-responsive.css')}}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php $SQLdatosUsuario = DB::table('persona')->select('*')->where('idpersona', session('idpersona'))->get(); ?>

  <section id="container" class="">
      <!--header start-->
      <header class="header white-bg">
          <div class="sidebar-toggle-box">
              <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
          </div>
          <!--logo start-->
          <a href="index.html" class="logo" >Sistema de Control - <span>RioDeco</span></a>
          <!--logo end-->


          <div class="top-nav ">
              <ul class="nav pull-right top-menu">
                  <!-- user login dropdown start-->
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <img alt="" src="{{URL::to('/resources/template/img/avatar1_small.jpg')}}">
                          <span class="username"><?php echo $SQLdatosUsuario[0]->nombre." ".$SQLdatosUsuario[0]->apellido; ?></span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                          <div class="log-arrow-up"></div>
                          <li><a href="#"><i class=" fa fa-suitcase"></i>Perfil</a></li>
                          <li><a href="logout"><i class="fa fa-key"></i>Salir</a></li>
                      </ul>
                  </li>
                  <!-- user login dropdown end -->
              </ul>
          </div>
      </header>
      <!--header end-->

      <!-- //Menu de Deposito -->
      <aside>
      <div id="sidebar"  class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
         
            <!--DISCRIMINAMOS LAS OPCIONES DEL MENU EN BASE A LOS PERFILES CARGADOS AL USUARIO-->
            
            @foreach ($perfiles as $perfil)
              

              @if($perfil->nombre == 'Deposito')

                <li><a  href="{{ URL::to('ordenes_de_compra') }}" target="marco">Ordenes de Compra</a></li>
                <li><a  href="{{ URL::to('/') }}" target="marco">Remitos</a></li>
                <li class="sub-menu">
                    <a  href="boxed_page.html">Control Stock</a>
                    <ul class="sub">
                        <li><a  href="{{ URL::to('materia_prima') }}" target="marco">Materia Prima</a></li>
                        <li><a  href="{{ URL::to('productos') }}" target="marco">Productos</a></li>
                        
                    </ul>
                </li>

              @endif

              @if($perfil->nombre == 'Planta')
                <li><a  href="{{ URL::to('ordenes_de_compra') }}" target="marco">Ordenes de Fabricacion</a></li>
                <li><a  href="{{ URL::to('/') }}" target="marco">Remitos</a></li>
                <li class="sub-menu">
                    <a  href="boxed_page.html">Control Stock</a>
                    <ul class="sub">
                        <li><a  href="{{ URL::to('/') }}" target="marco">Materia Prima</a></li>
                        <li><a  href="{{ URL::to('/') }}" target="marco">Productos</a></li>
                        
                    </ul>
                </li>
              @endif

             @endforeach
              <!--multi level menu end-->

          </ul>
          <!-- sidebar menu end-->
      </div>
  </aside>


      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
           

              <iframe src="{{URL::to('/bienvenida')}}" width="100%" height="900" name="marco" style="border:none">
                
               
              </iframe>
              
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2019 &copy; Lahitte Soluciones Informáticas. Todos los derechos reservados
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{URL::to('/resources/template/js/jquery.js')}}"></script>
    <script src="{{URL::to('/resources/template/js/bootstrap.min.js')}}"></script>
    <script class="include" type="text/javascript" src="{{URL::to('/resources/template/js/jquery.dcjqaccordion.2.7.js')}}"></script>
    <script src="{{URL::to('/resources/template/js/jquery.scrollTo.min.js')}}"></script>
    <script src="{{URL::to('/resources/template/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('/resources/template/js/respond.min.js')}}" ></script>

    <!--common script for all pages-->
    <script src="{{URL::to('/resources/template/js/common-scripts.js')}}"></script>

  </body>
</html>
