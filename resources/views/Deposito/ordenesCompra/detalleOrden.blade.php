@extends('plantillaVistas')

@section('titulo')
    Detalle de Compra
@endsection

@section('seccionCss')
    <link href="{{URL::to('/resources/template/assets/advanced-datatable/media/css/demo_page.css')}}" rel="stylesheet" />
    <link href="{{URL::to('/resources/template/assets/advanced-datatable/media/css/demo_table.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::to('/resources/template/assets/data-tables/DT_bootstrap.css')}}" />
@endsection
  

@section('seccionBody')
  <section id="container" class="">   
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Detalle de Compra 002
                          </header>
                          <div class="panel-body">
                              <p>Proveedor: Altos del Sur</p>
                              <p>Fecha compra: 22/10/2018</p>
                          </div>
                          <hr>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th>ID</th>
                                          <th>Producto</th>
                                          <th>Cantidad</th>
                                          <th>Terminados</th>
                                          <th>Retirados</th>
                                          <th class="">Porcentaje</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      
                                      <tr class="gradeX">
                                          <td>006</td>
                                          <td>Panel N39</td>
                                          <td>20</td>
                                          <td>10</td>
                                          <td>5</td>
                                          <td class="center hidden-phone">
                                            <div class="progress progress-striped active">
                                                <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentaje }}%">
                                                    <span class="sr-only" style="position: unset;">{{ $porcentaje }}% Completado</span>
                                                </div>
                                            </div>
                                          </td>
                                      </tr>
                                      
                                      </tbody>
                          </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
         
      <!--footer end-->
  </section>

@endsection

@section('seccionScripts')
    <script type="text/javascript" language="javascript" src="{{URL::to('/resources/template/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('/resources/template/assets/data-tables/DT_bootstrap.js')}}"></script>
    

    <!--script for this page only-->

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#example').dataTable( {
                "aaSorting": [[ 4, "desc" ]]
            } );
        } );
    </script>
@endsection