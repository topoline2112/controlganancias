@extends('plantillaVistas')


@section('titulo')
    Ordenes de Fabricacion
@endsection

@section('seccionCss')
    <link href="{{URL::to('/resources/template/assets/advanced-datatable/media/css/demo_page.css')}}" rel="stylesheet" />
    <link href="{{URL::to('/resources/template/assets/advanced-datatable/media/css/demo_table.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::to('/resources/template/assets/data-tables/DT_bootstrap.css')}}" />
@endsection
  

@section('seccionBody')
  <section id="container" class="">   
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Ordenes de Fabricación
                          </header>

                          <!--SOLAMENTE ESTE SECTOR PUEDE CREAR NUEVAS ORDENES DE FABRICACION -->
                          @if(session('sector') == 'Presupuesto')
                            <div class="panel-body">
                                <button type="button" class="btn btn-shadow btn-primary">Generar Nueva</button>
                            </div>
                          @endif

                          <div class="panel-body">
                                <div class="adv-table">
                                    <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th>ID</th>
                                          <th>Fecha Emision</th>
                                          <th>Generada por</th>
                                          <th class="hidden-phone">Porcentaje</th>
                                          <th class="hidden-phone">Estado</th>
                                          <th class="hidden-phone">Acciones</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">
                                            <div class="progress progress-striped active">
                                                <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentaje }}%">
                                                    <span class="sr-only" style="position: unset;">{{ $porcentaje }}% Completado</span>
                                                </div>
                                            </div>
                                          </td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="{{ URL::to('detalle_de_compra') }}" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      <tr class="gradeX">
                                          <td>002</td>
                                          <td>28/02/2019</td>
                                          <td>Alejandro Paez - Encargado de Produccion</td>
                                          <td class="center hidden-phone">En proceso</td>
                                          <td class="center hidden-phone">
                                            <a href="" title="Ver detalle"><button class="btn btn-warning btn-xs"><i class=" fa fa-search"></i></button></a>
                                            <!--
                                            <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                             -->
                                            <a href="" title="Eliminar"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                          </td>
                                      </tr>
                                      </tbody>
                                      <tfoot>
                                      <tr>
                                          <th>ID</th>
                                          <th>Fecha Emision</th>
                                          <th>Generada por</th>
                                          <th class="hidden-phone">Estado</th>
                                          <th class="hidden-phone">Acciones</th>
                                      </tr>
                                      </tfoot>
                          </table>
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
         
      <!--footer end-->
  </section>

@endsection

@section('seccionScripts')
    <script type="text/javascript" language="javascript" src="{{URL::to('/resources/template/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('/resources/template/assets/data-tables/DT_bootstrap.js')}}"></script>
    

    <!--script for this page only-->

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#example').dataTable( {
                "aaSorting": [[ 4, "desc" ]]
            } );
        } );
    </script>
@endsection