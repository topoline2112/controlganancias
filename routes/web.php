<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\DB; //Base de datos la clase
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| get
|--------------------------------------------------------------------------
|
*/


//INGRESAR AL LOGIN DEL SISTEMA

Route::get('/', function () {
	if (session()->has('sector')) { //Si ya esta iniciada la sesion
	    if (session('sector') == 'SU') {
	        return view('SuperUser/home');
	    }
	    if (session('sector') == 'secretaria') {
	        return view('Secretaria/home');
	    }
	    if (session('sector') == 'direccion') {
	        return view('Direccion/home');
	    }
	    if (session('sector') == 'playa') {
	        return view('Playa/home');
	    }
	}else{
		return view('login');
	}
    
});


//INICIO
Route::get('/bienvenida', 'ControladorLogin@bienvenida');
Route::get('logout', 'ControladorLogin@logout');


//INICIO RUTAS PARA DEPOSITO

	//SECCION ORDENES DE COMPRA
	Route::get('ordenes_de_compra','ControladorDeposito@verOrdenesDeCompra');

	Route::get('detalle_de_compra','ControladorDeposito@verDetalleDeCompra');
	//FIN SECCION

	//SECCION MATERIAS PRIMA
	Route::get('materia_prima','ControladorDeposito@verMateriaPrima');
	//FIN SECCION

	//SECCION PRODUCTOS
	Route::get('productos','ControladorDeposito@verProductos');
	//FIN SECCION

//FIN RUTAS PARA DEPOSITO



/*
|----------------------------------------------------------€----------------
| post
|--------------------------------------------------------------------------
|
*/


Route::post('/inicio', 'ControladorLogin@intentoLogueo');
